package com.example.newbe.common;

import com.example.newbe.constant.AppConstant;
import com.example.newbe.model.enums.AppEnum;
import com.example.newbe.utils.MessageUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
public class AppResponse<T> {
    private T data;
    private int total;
    private int code;
    private String message;

    @DateTimeFormat(pattern = AppConstant.DATE_FORMAT)
    private LocalDate date = LocalDate.now();

    public AppResponse(T data, Integer total, Integer code, String message) {
        this.data = data;
        this.total = total;
        this.code = code;
        this.message = message;
    }

    public AppResponse(T data, AppEnum enums) {
        this.data = data;
        this.code = enums.getCode();
        this.message = enums.getMessage();
        this.total = data instanceof Collection ? ((Collection<?>) data).size() : 0;
    }

    public static <T> AppResponse<T> build(T data) {
        AppResponse<T> response = new AppResponse<>();
        response.data = data;
        response.code = HttpStatus.OK.value();
        response.message = MessageUtils.get("message.ok", new Object[0]);
        response.total = data instanceof Collection ? ((Collection<?>) data).size() : 0;
        return response;
    }

    public static <T> AppResponse<T> build(int code, String messageUrl, Object... args) {
        AppResponse<T> response = new AppResponse<>();
        response.code = code;
        response.message = MessageUtils.get(messageUrl, args);
        return response;
    }

    public static <T> AppResponse<T> buildMsg(int code, String message) {
        AppResponse<T> response = new AppResponse<>();
        response.code = code;
        response.message = message;
        return response;
    }

    public static <T> AppResponse<T> buildAppException(String message, HttpStatus httpStatus) {
        AppResponse<T> response = new AppResponse<>();
        response.code = httpStatus.value();
        response.message = message + ", ERROR: " + httpStatus.getReasonPhrase();
        return response;
    }
}
