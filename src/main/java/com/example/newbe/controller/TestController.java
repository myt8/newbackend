package com.example.newbe.controller;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/test")
public class TestController {
    private final ModelMapper modelMapper;

    /**
     * Cách viết này có thể sử dụng 2 api cùng một lúc
     * Có thể dùng /convert và cũng có thể là /post
     */
    @PostMapping(value = {"/convert", "/post"})
    public void whenConvertPostEntity() {

    }
}
