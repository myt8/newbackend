package com.example.newbe.controller;

import com.example.newbe.common.AppResponse;
import com.example.newbe.model.request.UserRequest;
import com.example.newbe.model.response.UserResponse;
import com.example.newbe.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(name = "/user")
public class UserController {
    private final UserService userService;

    @PostMapping
    public AppResponse<UserResponse> create(@RequestBody UserRequest request) {
        return userService.create(request);
    }

    @GetMapping("{id}")
    public AppResponse<UserResponse> getBy(@PathVariable(name = "id") Long id) {
        return userService.getBy(id);
    }

    @DeleteMapping("{id}")
    public AppResponse<?> delete(@PathVariable(name = "id") Long id) {
        return userService.delete(id);
    }

    @PutMapping("{id}")
    public AppResponse<UserResponse> update(@PathVariable(name = "id") Long id,
                                            @RequestBody UserRequest request) {
        return userService.update(id, request);
    }
}
