package com.example.newbe.controller;

import com.example.newbe.common.AppResponse;
import com.example.newbe.model.request.ChildRequest;
import com.example.newbe.model.response.ChildResponse;
import com.example.newbe.search.ChildSearch;
import com.example.newbe.service.ChildService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/child")
public class ChildController {

    private final ChildService childService;

    @PostMapping
    public AppResponse<List<ChildResponse>> create(@RequestBody List<ChildRequest> requests) {
        return childService.createBy(requests);
    }

    @PostMapping("/search")
    public AppResponse<List<ChildResponse>> searchBy(@RequestBody ChildSearch search) {
        return childService.searchBy(search);
    }
}
