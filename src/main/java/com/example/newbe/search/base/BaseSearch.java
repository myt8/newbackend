package com.example.newbe.search.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseSearch {
    private int pageIndex = 0;
    private int pageSize = 10;
}
