package com.example.newbe.search;

import com.example.newbe.model.enums.GenderEnum;
import com.example.newbe.search.base.BaseSearch;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ChildSearch extends BaseSearch {
    private String name;

    private Integer age;

    private Date birthDate;

    private GenderEnum genderEnum;
}
