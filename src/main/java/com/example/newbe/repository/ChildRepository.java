package com.example.newbe.repository;

import com.example.newbe.entity.Child;
import com.example.newbe.search.ChildSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChildRepository extends JpaRepository<Child, Long> {

    @Query(value = "SELECT * FROM tbl_child c WHERE c.name = :#{#search.name} AND c.age = :#{#search.age}",
            countQuery = "SELECT count(*) FROM tbl_child c WHERE c.name = :#{#search.name} AND c.age = :#{#search.age}",
            nativeQuery = true)
    List<Child> findAllByNameAndAge(@Param("search") ChildSearch search,
                                    Pageable pageable);
}
