package com.example.newbe.generics;

import org.apache.commons.lang3.ObjectUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Generics {

    /**
     * Convert array to list ( Chuyển đổi mảng sang danh sách)
     *
     * @param array tham số
     * @return một danh sách
     */
    public <T extends Arrays> List<T> fromArrayToList(T[] array) {
        return Arrays.stream(array).collect(Collectors.toList());
    }

    public <E> boolean isNotEmpty(E[] array) {
        return ObjectUtils.isNotEmpty(array);
    }

    public <E> boolean isContain(E[] array) {
        if (null != array && isNotEmpty(array)) {
            return Arrays.asList(array).contains("");
        }
        return false;
    }

    public static <T, G> List<G> fromArrayToList(T[] array, Function<T, G> mapperFunction) {
        return Arrays.stream(array).map(mapperFunction).collect(Collectors.toList());
    }
}
