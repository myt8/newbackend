package com.example.newbe.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class TestHelper {

    public static void main(String[] args) throws JsonProcessingException {
        Stream<Integer> integerStream = Stream.of(1, 2, 4, 5, 6, 7, 8, 9, 10);
        integerStream.filter(i -> i != null)
                .skip(3)
                .limit(3)
                .forEach(System.out::println);
    }
}
