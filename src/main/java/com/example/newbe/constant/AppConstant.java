package com.example.newbe.constant;

public interface AppConstant {
    int INVALID_VALUE = 400;
    String DATE_FORMAT = "dd/MM/yyy HH:mm:ss";

    String ID_NOT_FOUND = "Id not found";
    String NOT_FOUND = "Not found";
}
