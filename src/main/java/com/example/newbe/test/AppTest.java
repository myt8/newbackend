package com.example.newbe.test;

import com.example.newbe.generics.Generics;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.hasItems;

@Test
public class AppTest {

    public void givenArrayOfIntegers() {
        Integer[] intArray = {1, 2, 3, 4, 5};
        List<String> stringList = Generics.fromArrayToList(intArray, Object::toString);
        Assert.assertEquals(stringList, hasItems("1", "2", "3", "4", "5"));
    }

    public void testStreamToMap() throws JsonProcessingException {
        List<String> listWithDuplicates = Arrays.asList("a", "bb", "c", "d", "bb");
        Map<String, Integer> result = listWithDuplicates.stream()
                .collect(toMap(Function.identity(), String::length));
        new ObjectMapper().writeValueAsString(result);
    }
}
