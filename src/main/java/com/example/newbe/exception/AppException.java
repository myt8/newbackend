package com.example.newbe.exception;

import com.example.newbe.common.AppResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@Slf4j
@RestControllerAdvice
public class AppException {
    @ExceptionHandler(value = {IOException.class})
    public @ResponseBody ResponseEntity<Object> handleIOException(IOException ex) {
        log.error("Invalid IOException: {}", ex.getMessage(), ex);
        return buildResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ResponseEntity<Object> handleException(Exception ex) {
        log.error("Internal server error: {}", ex.getMessage(), ex);
        return buildResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error("Invalid MethodArgumentNotValidException: {}", ex.getMessage(), ex);
        return buildResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private @ResponseBody ResponseEntity<Object> buildResponse(String message, HttpStatus status) {
        return new ResponseEntity<>(AppResponse.buildMsg(status.value(), message), status);
    }

    private @ResponseBody ResponseEntity<Object> buildResponseV2(String message, HttpStatus status) {
        return new ResponseEntity<>(AppResponse.buildAppException(message, status), status);
    }
}
