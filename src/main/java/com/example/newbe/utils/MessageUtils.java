package com.example.newbe.utils;

import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

@Component
@NoArgsConstructor
public class MessageUtils {
    private static final Logger log = LoggerFactory.getLogger(MessageUtils.class);
    private static MessageSource source;
    @Autowired
    private MessageSource messageSource;

    public static String get(Locale locale, String code, @Nullable Object[] args) {
        try {
            return source.getMessage(code, args, locale);
        } catch (Exception ex) {
            log.info(ex.getMessage());
            return code;
        }
    }

    public static String get(String code, @Nullable Object... args) {
        return get(LocaleContextHolder.getLocale(), code, args);
    }

    @PostConstruct
    private void setMessageSource() {
        source = this.messageSource;
    }
}
