package com.example.newbe.utils;

import java.util.List;
import java.util.stream.Stream;

public class StreamUtils {

    public Stream<String> streamOf(List<String> list) {
        return list == null || list.isEmpty() ? Stream.empty() : list.stream();
    }
}
