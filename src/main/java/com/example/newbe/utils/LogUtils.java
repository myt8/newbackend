package com.example.newbe.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogUtils {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static <T> String toJson(T required) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(required);
        } catch (JsonProcessingException exception) {
            log.error("Convert Object To Json Fail! ERROR = {}", exception.getMessage(), exception);
        }
        return "";
    }
}
