package com.example.newbe.mapper;

import com.example.newbe.entity.User;
import com.example.newbe.model.request.UserRequest;
import com.example.newbe.model.response.UserResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserResponse mapperToResponse(User user);

    User mapperToEntity(UserRequest request);
}
