package com.example.newbe.model.response;

import com.example.newbe.model.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ChildResponse {
    private String name;

    private Integer age;

    private Date birthDate;

    private GenderEnum genderEnum;
}
