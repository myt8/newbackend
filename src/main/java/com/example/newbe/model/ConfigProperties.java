package com.example.newbe.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@NoArgsConstructor
@Configuration
//@PropertySource(value = "classpath:config.xml")
@ConfigurationProperties(prefix = "mail.credentials")
public class ConfigProperties {
    private String hostName;
    private int port;
    private String from;

    @ConstructorBinding
    public ConfigProperties(String hostName, int port, String from) {
        this.hostName = hostName;
        this.port = port;
        this.from = from;
    }
}
