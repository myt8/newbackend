package com.example.newbe.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
@AllArgsConstructor
public enum GenderEnum {
    MALE(1, "Name"),
    FEMALE(2, "Nữ");

    private final int code;
    private final String name;

    @JsonCreator
    public static boolean getByCodeOfName(@JsonProperty("code") int code,
                                          @JsonProperty("name") String name) {
        return Arrays.stream(values())
                .filter(Objects::nonNull)
                .map(x -> Objects.equals(x, code) || Objects.equals(x, name))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Invalid argument"));
    }
}
