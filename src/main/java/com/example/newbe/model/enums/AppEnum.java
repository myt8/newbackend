package com.example.newbe.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppEnum {
    ERROR_MESSAGE(400, "");
    private final int code;
    private final String message;
}
