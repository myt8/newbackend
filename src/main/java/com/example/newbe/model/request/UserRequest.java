package com.example.newbe.model.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    @NotNull(message = "Tên Không Được Null")
    @Size(max = 36)
    String username;

    @NotNull(message = "Mật Khẩu Không Được Null")
    @Size(max = 36)
    String password;
}
