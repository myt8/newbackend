package com.example.newbe.model.request;

import com.example.newbe.model.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ChildRequest {
    private String name;

    private Integer age;

    private Date birthDate;

    private GenderEnum genderEnum;
}
