package com.example.newbe.entity;

import com.example.newbe.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_role", schema = "testspringboot")
public class Role extends BaseEntity {
    @Column(name = "role_name", length = 150, nullable = false)
    private String name;

    /**
     * @Transient muốn tạo 1 trường không bền vững. Nó chỉ định rằng trường sẽ không được duy trì
     * Transient: Tạm thời
     */
    @Transient
    @Column(name = "role_description", length = 512)
    @Size(max = 512, message = "{validation.forms.role.description.size}")
    private String description;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();
}
