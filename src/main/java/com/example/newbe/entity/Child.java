package com.example.newbe.entity;

import com.example.newbe.entity.base.BaseEntity;
import com.example.newbe.model.enums.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_child", schema = "testspringboot")
public class Child extends BaseEntity {
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Transient
    @Column(name = "age")
    private Integer age;

    @Temporal(TemporalType.DATE)
    @Column(name = "birthDate")
    private Date birthDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender_enum")
    private GenderEnum genderEnum;
}
