package com.example.newbe.entity;

import com.example.newbe.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_address", schema = "testspringboot")
public class Address extends BaseEntity {
    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "code", length = 36)
    private String code;
}
