package com.example.newbe.entity;

import com.example.newbe.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_group", schema = "testspringboot")
public class Group extends BaseEntity {
    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "groups")
    private Set<User> users = new HashSet<>();
}
