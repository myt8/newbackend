package com.example.newbe.entity.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "created_by")
    private String createdBy; // Người tạo ra

    @Column(name = "updated_by")
    private String updatedBy; // Cập nhật bởi

    @CreatedDate
    @Column(name = "created_at", updatable = false)
    private LocalDate createdAt; // Ngày tạo

    @LastModifiedDate
    @Column(name = "updated_at")
    private LocalDate updatedAt; // Ngày cập nhật
}
