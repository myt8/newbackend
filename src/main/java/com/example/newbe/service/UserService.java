package com.example.newbe.service;

import com.example.newbe.common.AppResponse;
import com.example.newbe.model.request.UserRequest;
import com.example.newbe.model.response.UserResponse;
import lombok.NonNull;

public interface UserService {
    AppResponse<UserResponse> create(@NonNull UserRequest request);

    AppResponse<UserResponse> getBy(@NonNull Long id);

    AppResponse<Boolean> delete(@NonNull Long id);

    AppResponse<UserResponse> update(@NonNull Long id, @NonNull UserRequest request);
}
