package com.example.newbe.service;

import com.example.newbe.common.AppResponse;
import com.example.newbe.model.request.ChildRequest;
import com.example.newbe.model.response.ChildResponse;
import com.example.newbe.search.ChildSearch;
import lombok.NonNull;

import java.util.List;

public interface ChildService {

    AppResponse<List<ChildResponse>> createBy(@NonNull List<ChildRequest> requests);
    AppResponse<List<ChildResponse>> searchBy(@NonNull ChildSearch search);
}
