package com.example.newbe.service.impl;

import com.example.newbe.common.AppResponse;
import com.example.newbe.entity.User;
import com.example.newbe.exception.RunTimeException;
import com.example.newbe.mapper.UserMapper;
import com.example.newbe.model.request.UserRequest;
import com.example.newbe.model.response.UserResponse;
import com.example.newbe.repository.UserRepository;
import com.example.newbe.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

import static com.example.newbe.constant.AppConstant.INVALID_VALUE;
import static com.example.newbe.constant.AppConstant.NOT_FOUND;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Transactional
    @Override
    public AppResponse<UserResponse> create(@NonNull UserRequest request) {
        User user = userRepository.save(modelMapper.map(request, User.class));
        return AppResponse.build(modelMapper.map(user, UserResponse.class));
    }

    @Override
    public AppResponse<UserResponse> getBy(@NonNull Long id) {
        if (!userRepository.existsById(id)) {
            log.error("Error get data user fail! Not found Id = {}", id);
            return AppResponse.build(null);
        }
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.map(entity -> AppResponse.build(modelMapper.map(entity, UserResponse.class)))
                .orElseGet(() -> AppResponse.build(INVALID_VALUE, NOT_FOUND));
    }

    @Override
    public AppResponse<Boolean> delete(@NonNull Long id) {
        if (!userRepository.existsById(id)) {
            log.error("Could not delete user because not found id = {}", id);
            return AppResponse.build(false);
        }
        userRepository.deleteById(id);
        return AppResponse.build(true);
    }

    @Transactional
    @Override
    public AppResponse<UserResponse> update(@NonNull Long id, @NonNull UserRequest request) {
        User user = userRepository.findById(id).orElseThrow(() -> new RunTimeException(NOT_FOUND));
        modelMapper.map(request, user);
        return AppResponse.build(modelMapper.map(user, UserResponse.class));
    }
}
