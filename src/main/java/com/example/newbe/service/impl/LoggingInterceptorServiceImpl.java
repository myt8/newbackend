package com.example.newbe.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Spring RestTemplate request / response Logging
 */
@Service
public class LoggingInterceptorServiceImpl implements ClientHttpRequestInterceptor {

    static Logger logger = LoggerFactory.getLogger(LoggingInterceptorServiceImpl.class);

    /**
     * @param request   the request, containing method, URI, and headers
     * @param reqBody   the body of the request
     * @param execution the request execution
     * @return the response
     * @throws IOException in case of I/O errors
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] reqBody, ClientHttpRequestExecution execution) throws IOException {
        logger.debug("Request body: {}", new String(reqBody, StandardCharsets.UTF_8));
        ClientHttpResponse response = execution.execute(request, reqBody);
        InputStreamReader isr = new InputStreamReader(response.getBody(), StandardCharsets.UTF_8);
        String body = new BufferedReader(isr).lines().collect(Collectors.joining("\n"));
        logger.debug("Response body: {}", body);
        return response;
    }

    public static void main(String[] args) {
        String a = "Hôm này trời đẹp";
        String collect = String.join("\n", List.of(a));
        System.out.println("Ky tu: " + collect);
    }
}






















