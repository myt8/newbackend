package com.example.newbe.service.impl;

import com.example.newbe.common.AppResponse;
import com.example.newbe.entity.Child;
import com.example.newbe.mapper.modelmapper.ListMapper;
import com.example.newbe.model.request.ChildRequest;
import com.example.newbe.model.response.ChildResponse;
import com.example.newbe.repository.ChildRepository;
import com.example.newbe.search.ChildSearch;
import com.example.newbe.service.ChildService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ChildServiceImpl implements ChildService {

    private final ChildRepository childRepository;
    private final ListMapper listMapper;

    @Override
    public AppResponse<List<ChildResponse>> createBy(@NonNull List<ChildRequest> requests) {
        List<Child> children = listMapper.mapList(requests, Child.class);
        List<ChildResponse> responses = listMapper.mapList(children, ChildResponse.class);

        return AppResponse.build(responses);
    }

    @Override
    public AppResponse<List<ChildResponse>> searchBy(@NonNull ChildSearch search) {
        PageRequest pageRequest = PageRequest.of(search.getPageIndex(), search.getPageSize());
        List<Child> entities = childRepository.findAllByNameAndAge(search, pageRequest);
        List<ChildResponse> childResponses = listMapper.mapList(entities, ChildResponse.class);
        return AppResponse.build(childResponses);
    }
}
